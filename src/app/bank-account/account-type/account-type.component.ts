import { Component, OnInit } from '@angular/core';
// import {NgbAccordionConfig} from '@ng-bootstrap/ng-bootstrap';
// import {MatExpansionModule} from '@angular/material/expansion';


import { trigger, state, style, animate, transition } from '@angular/animations';

@Component({
  selector: 'app-account-type',
  templateUrl: './account-type.component.html',
  styleUrls: ['./account-type.component.scss'],
  animations: [
    trigger('smoothCollapse', [
      state('initial', style({
        height:'0',
        overflow:'hidden'
      })),
      state('final', style({
        overflow:'hidden'
            })),
      transition('initial=>final', animate('200ms')),
      transition('final=>initial', animate('200ms'))
    ]),
  ]
})
export class AccountTypeComponent implements OnInit {

  ngOnInit(): void {


  }


}

