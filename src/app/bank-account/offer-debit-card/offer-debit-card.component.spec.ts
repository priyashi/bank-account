import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OfferDebitCardComponent } from './offer-debit-card.component';

describe('OfferDebitCardComponent', () => {
  let component: OfferDebitCardComponent;
  let fixture: ComponentFixture<OfferDebitCardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OfferDebitCardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OfferDebitCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
