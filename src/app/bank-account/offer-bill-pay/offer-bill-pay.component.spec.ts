import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OfferBillPayComponent } from './offer-bill-pay.component';

describe('OfferBillPayComponent', () => {
  let component: OfferBillPayComponent;
  let fixture: ComponentFixture<OfferBillPayComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OfferBillPayComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OfferBillPayComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
