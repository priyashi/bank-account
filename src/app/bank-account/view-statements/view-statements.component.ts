import { Component, OnInit, ViewChild } from '@angular/core';

import { trigger, state, style, animate, transition } from '@angular/animations';

@Component({
  selector: 'app-view-statements',
  templateUrl: './view-statements.component.html',
  styleUrls: ['./view-statements.component.scss'],
  animations: [
    trigger('smoothCollapse', [
      state('initial', style({
        height:'0',
        overflow:'hidden'
      })),
      state('final', style({
        overflow:'hidden',
      })),
      transition('initial=>final', animate('600ms')),
      transition('final=>initial', animate('600ms'))
    ]),
  ]
})



export class ViewStatementsComponent implements OnInit {
  public isShowDivIf = false;
  public filterisShowDivIf = false;
 Collapsed = true;

  @ViewChild("email_filter_open") lab;
  @ViewChild("filter_open") filter_lab;

  // @ViewChild(AccountDetailsComponent);

//   model: NgbDateStruct;
//   constructor(config: NgbInputDatepickerConfig, calendar: NgbCalendar) {
//     // days that don't belong to current month are not visible
//     config.outsideDays = 'hidden';

//     // weekends are disabled
//     config.markDisabled = (date: NgbDate) => calendar.getWeekday(date) >= 6;

//     // setting datepicker popup to close only on click outside
//     config.autoClose = 'outside';

//     // setting datepicker popup to open above the input
//     config.placement = ['top-left', 'top-right'];
// }

  ngOnInit(): void {


  }

  status: boolean = false;
  active_click(){
      this.status = !this.status;       
// let open_acc = document.getElementById("open_acc_more_details");

      if(this.status) {
        // Collapsed_acc_details = true;

      // open_acc.
      //   open_acc.nativeElement.classList.remove("hide");
      }
  }

  togglefilterDisplayDivIf() {
    this.filterisShowDivIf = !this.filterisShowDivIf;
    this.isShowDivIf = false;

    if(this.filterisShowDivIf) {
      this.filter_lab.nativeElement.classList.add("show");
      this.filter_lab.nativeElement.classList.remove("hide");

    } else {
      this.filter_lab.nativeElement.classList.add("hide");
      this.filter_lab.nativeElement.classList.remove("show");
    }
  }

  toggleDisplayDivIf() {
    this.isShowDivIf = !this.isShowDivIf;
  this.filterisShowDivIf = false;

    if(this.isShowDivIf) {
      this.lab.nativeElement.classList.add("show");
      this.lab.nativeElement.classList.remove("hide");
    } else {
      this.lab.nativeElement.classList.add("hide");
      this.lab.nativeElement.classList.remove("show");
    }
  }

  }



