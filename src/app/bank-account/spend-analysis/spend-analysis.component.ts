import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { ChartDataSets, ChartOptions } from 'chart.js';
import { Color, Label } from 'ng2-charts';
import * as pluginAnnotations from 'chartjs-plugin-annotation';

@Component({
  selector: 'app-spend-analysis',
  templateUrl: './spend-analysis.component.html',
  styleUrls: ['./spend-analysis.component.scss']
})
export class SpendAnalysisComponent implements OnInit {

  // @ViewChild("myCanvas") canvas: ElementRef;
  // lineChartColors;
  lineChartData: ChartDataSets[] = [
    { data: [25, 52, 18, 55, 75]},
  ];

  lineChartLabels: Label[] = ['Dec', 'Jan', 'Feb', 'Mar', 'Apr'];

  public lineChartOptions: (ChartOptions & { annotation: any }) = {
    responsive: true,

    elements: {
      point: {
          radius: 0
      }
  },
    scales: {

      // We use this empty structure as a placeholder for dynamic theming.
      xAxes: [{
       gridLines: { display: false } ,
       ticks: {
        fontColor: '#898B90',
        fontSize: 10
      }


      }],
      yAxes: [
  {
    display: false
  }
      ]
    },
    annotation: {
      annotations: [
        {
          type: 'line',
          mode: 'vertical',
          scaleID: 'x-axis-0',
          value: 'Mar',
          borderWidth: 1,
          yMax: 5,
          yMin: 4,

          label: {
            enabled: true,
            fontColor: '#6695DB',
            content: '+3.25%',
            backgroundColor: '#E4ECF9',
            fontSize:10,
            borderRadius:10
          }
        },
      ],
    },
  };

lineChartColors: Color[] = [
    {
      borderColor: '#6695DB',
      backgroundColor: '#E4ECF9',
      borderWidth: 1,

    },
  ];

  lineChartLegend = false;
lineChartPlugins = [pluginAnnotations];
  lineChartType = 'line';
 

  constructor() {
  
  }

  ngOnInit() {
 
  //   const gradient = this.canvas.nativeElement.getContext('2d').createLinearGradient(0, 0, 0, 600);
  //   gradient.addColorStop(0, 'rgba(199, 150, 239, 0.1)');
  //   gradient.addColorStop(1, 'rgba(199, 150, 239, 1)');
  //   this.lineChartColors = [
  //       {
  //           backgroundColor: gradient
  //       }
  // ];
  }



}