import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-goals',
  templateUrl: './goals.component.html',
  styleUrls: ['./goals.component.scss']
})
export class GoalsComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

  //collapse for services
  ServiceCollapsed = true;

    //collapse for goals
  GoalsCollapsed = true;

  // tab alignment for goals in vertical
  currentOrientation = 'vertical';

    // increase goal div
  increase_div: boolean = false;

  // add class show and hide on clicking goal collapse
  status: boolean = true;

  // function for all goal actions
  status_click(){
      this.status = !this.status;    
      this.increase_div = !this.increase_div;   
  }

  // increase service div
  inc_service_div: boolean = false;

  open_service_div: boolean = true;

    // function for all services actions
    service_click(){
      this.inc_service_div = !this.inc_service_div;   
      this.open_service_div = !this.open_service_div;   

  }


}
