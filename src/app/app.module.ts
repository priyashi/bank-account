import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ChartsModule } from 'ng2-charts';




import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './Re-share/header/header.component';
import { SideNavComponent } from './Re-share/side-nav/side-nav.component';
import { ProfileComponent } from './bank-account/profile/profile.component';
import { AccountTypeComponent } from './bank-account/account-type/account-type.component';
import { AccountDetailsComponent } from './bank-account/account-details/account-details.component';
import { GoalsComponent } from './bank-account/goals/goals.component';
import { ViewStatementsComponent } from './bank-account/view-statements/view-statements.component';
import { RecentTransactionsComponent } from './bank-account/recent-transactions/recent-transactions.component';
import { CardDetailsComponent } from './bank-account/card-details/card-details.component';
import { OfferDebitCardComponent } from './bank-account/offer-debit-card/offer-debit-card.component';
import { OfferBillPayComponent } from './bank-account/offer-bill-pay/offer-bill-pay.component';
import { SpendAnalysisComponent } from './bank-account/spend-analysis/spend-analysis.component';
import { BankAccountComponent } from './bank-account/bank-account.component';


@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    SideNavComponent,
    ProfileComponent,
    AccountTypeComponent,
    AccountDetailsComponent,
    GoalsComponent,
    ViewStatementsComponent,
    RecentTransactionsComponent,
    CardDetailsComponent,
    OfferDebitCardComponent,
    OfferBillPayComponent,
    SpendAnalysisComponent,
    BankAccountComponent,
 
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    NgbModule,
    BrowserAnimationsModule,
    ChartsModule
    ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
